package com.eduardorabanal.app.firstapp.t2.entities;

/**
 * Created by USER on 14/06/2017.
 */
public class Episodio {
    private String Nombre;

    public Episodio(String nombre, String link) {
        Nombre = nombre;
        Link = link;
    }

    public String getLink() {

        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    private String Link;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}

package com.eduardorabanal.app.firstapp.t2.entities;

/**
 * Created by USER on 14/06/2017.
 */
public class Anime {
    private String Imagen;
    private String Titulo;
    private String Texto;
    private String Id;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Anime(String imagen, String titulo, String texto, String id) {
        Imagen = imagen;
        Titulo = titulo;
        Texto = texto;

        Id = id;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getTexto() {
        return Texto;
    }

    public void setTexto(String texto) {
        Texto = texto;
    }
}

package com.eduardorabanal.app.firstapp.t2.adaptadores;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.t2.activities.AnimeActivity;
import com.eduardorabanal.app.firstapp.t2.entities.Anime;
import com.eduardorabanal.app.firstapp.t2.entities.Episodio;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by USER on 14/06/2017.
 */
public class AdapterEpisodio extends BaseAdapter {
    AppCompatActivity _activity;
    int _item_layout;
    List<Episodio> _datos;

    public AdapterEpisodio(
            AppCompatActivity appContext,
            int item_layout,
            List<Episodio> datos) {
        _activity = appContext;
        _item_layout = item_layout;
        _datos = datos;
    }

    @Override
    public int getCount() {
        return _datos.size();
    }

    @Override
    public Episodio getItem(int position) {
        return _datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = _activity.getLayoutInflater();
            convertView = vi.inflate(_item_layout, parent, false);
        }

        final Episodio obj = getItem(position);

        TextView tv_nombre = (TextView) convertView.findViewById(R.id.tv_nombre);
        tv_nombre.setText(obj.getNombre());

        TextView tv_link = (TextView) convertView.findViewById(R.id.tv_link);
        tv_link.setText(obj.getLink());

        Button btnVer = (Button) convertView.findViewById(R.id.btnVer);
        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_activity.getApplicationContext(),AnimeActivity.class);
                //intent.putExtra("animeId", obj.getId());
                //_activity.startActivity(intent);
                _activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(obj.getLink())));



                //_activity.startActivity(new Intent(_activity.getApplicationContext(),ContactoActivity.class));
            }
        });

        return convertView;
    }

    private void dialContactPhone(final String phoneNumber) {
        if (ActivityCompat.checkSelfPermission(_activity.getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(_activity,
                    new String[]{Manifest.permission.CALL_PHONE},
                    0);

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null)));
    }



    private void emailIntent(final String email){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        _activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }
}
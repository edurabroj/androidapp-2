package com.eduardorabanal.app.firstapp.t2.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp._practica.entities.Contacto;
import com.eduardorabanal.app.firstapp.t2.entities.Anime;
import com.eduardorabanal.app.firstapp.t2.tasks.AnimeTask;
import com.eduardorabanal.app.firstapp.t2.tasks.DownloadImageTask;

import org.json.JSONException;
import org.json.JSONObject;

public class AnimeActivity extends AppCompatActivity {
    ImageView iv;
    TextView tv_titulo ;
    TextView tv_texto ;
    TextView tv_nroEpisodios ;
    Button btnVerEpisodios;

    /*private int animeId;

    public AnimeActivity(int animeId) {
        this.animeId = animeId;
    }*/

    Bundle extras;
    String animeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anime);

        extras = this.getIntent().getExtras();
        animeId = extras.getString("animeId");

        iv = (ImageView) findViewById(R.id.imageView);
        tv_titulo = (TextView) findViewById(R.id.tv_titulo);
        tv_texto = (TextView) findViewById(R.id.tv_texto);
        tv_nroEpisodios = (TextView) findViewById(R.id.tv_nroEpisodios);
        btnVerEpisodios = (Button) findViewById(R.id.btnVerEpisodios);

        AnimeTask at = new AnimeTask(this,animeId);
        at.execute();




    }

    public void LlenarAnime(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        final Anime obj = GetDatosFromJson(respuesta);

        Log.i("RESPUESTA", obj.getImagen()+obj.getTitulo()+obj.getTexto());

        JSONObject item = new JSONObject(respuesta);


        tv_titulo.setText(item.getString("nombre"));
        tv_texto.setText(item.getString("descripcion"));
        tv_nroEpisodios.setText(item.getJSONArray("episodes").length()+"");


        DownloadImageTask it;
        it = new DownloadImageTask(iv);
        it.execute(obj.getImagen());

        btnVerEpisodios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),EpisodioListActivity.class);
                intent.putExtra("animeId", obj.getId());
                startActivity(intent);
            }
        });

        /*
        return new Anime(
                item.getString("imagen"),
                item.getString("nombre"),
                item.getString("descripcion"),
                item.getString("id")
        );

        //iv.setImageResource (obj.getImagen());
        tv_titulo.setText(obj.getTitulo());
        tv_texto.setText(obj.getTexto());
        tv_texto.setText(obj.getTexto());*/
    }

    private Anime GetDatosFromJson(String respuesta) throws JSONException {
        JSONObject item = new JSONObject(respuesta);
        return new Anime(
                item.getString("imagen"),
                item.getString("nombre"),
                item.getString("descripcion"),
                item.getString("id")
                );
    }
}

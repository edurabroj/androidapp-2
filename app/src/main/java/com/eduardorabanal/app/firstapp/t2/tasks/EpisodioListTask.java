package com.eduardorabanal.app.firstapp.t2.tasks;

import android.os.AsyncTask;

import com.eduardorabanal.app.firstapp.t2.activities.AnimeActivity;
import com.eduardorabanal.app.firstapp.t2.activities.AnimeListActivity;
import com.eduardorabanal.app.firstapp.t2.activities.EpisodioListActivity;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by USER on 14/06/2017.
 */
public class EpisodioListTask extends AsyncTask<String, Void, String> {
    private EpisodioListActivity activity;
    private String animeId;

    public EpisodioListTask(EpisodioListActivity activity, String animeId) {
        this.activity = activity;
        this.animeId = animeId;
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String respuesta = null;
        try {
            //configurar
            URL url = new URL("http://104.236.121.245/animes/" + animeId + "/episodios");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            //obtener datos
            InputStream is = connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            respuesta = r.readLine();
            r.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return respuesta;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            activity.LlenarEpisodios(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
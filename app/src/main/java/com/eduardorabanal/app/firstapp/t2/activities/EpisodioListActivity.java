package com.eduardorabanal.app.firstapp.t2.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.t2.adaptadores.AdapterAnime;
import com.eduardorabanal.app.firstapp.t2.adaptadores.AdapterEpisodio;
import com.eduardorabanal.app.firstapp.t2.entities.Anime;
import com.eduardorabanal.app.firstapp.t2.entities.Episodio;
import com.eduardorabanal.app.firstapp.t2.tasks.EpisodioListTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EpisodioListActivity extends AppCompatActivity {

    Bundle extras;
    String animeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodio_list);

        extras = this.getIntent().getExtras();
        animeId = extras.getString("animeId");

        EpisodioListTask et= new EpisodioListTask(this, animeId);
        et.execute();
    }

    public void LlenarEpisodios(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        ListView lv = (ListView) findViewById(R.id.lv);
        List<Episodio> datos=GetDatosFromJson(respuesta);

        AdapterEpisodio adaptador =
                new AdapterEpisodio(
                        this,
                        R.layout.item_episodio,
                        datos
                );

        lv.setAdapter(adaptador);
    }

    private List<Episodio> GetDatosFromJson(String respuesta) throws JSONException {
        JSONObject rpta = new JSONObject(respuesta);

        List<Episodio> datos = new ArrayList<>();
        JSONArray array = rpta.getJSONArray("episodes");

        for (int i=0; i<array.length(); i++){
            JSONObject item = array.getJSONObject(i);

            /*int id = getResources().getIdentifier(item.getString("img"), "mipmap", this.getPackageName());
            Log.i("ID",String.valueOf(id));*/

            datos.add(new Episodio(item.getString("nombre"),item.getString("link")));
        }

        return datos;
    }
}

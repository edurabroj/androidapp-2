package com.eduardorabanal.app.firstapp._practica.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp._practica.entities.Contacto;
import com.eduardorabanal.app.firstapp._practica.tasks.ContactoTask;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactoActivity extends AppCompatActivity {
    ImageView iv;
    TextView tv_nombre ;
    TextView tv_telf ;
    TextView tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);

        iv = (ImageView) findViewById(R.id.imageView);
        tv_nombre = (TextView) findViewById(R.id.tv_nombre);
        tv_telf = (TextView) findViewById(R.id.tv_telf);
        tv_email = (TextView) findViewById(R.id.tv_email);

        ContactoTask ct = new ContactoTask(this);
        ct.execute();
    }

    public void LlenarContacto(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        Contacto contacto = GetDatosFromJson(respuesta);

        Log.i("IV",String.valueOf(iv));
        Log.i("CONTACTO", contacto.getImg()+contacto.getNombre()+contacto.getTelefono()+contacto.getCorreo());

        iv.setImageResource (contacto.getImg());
        tv_nombre.setText(contacto.getNombre());
        tv_telf.setText(contacto.getTelefono());
        tv_email.setText(contacto.getCorreo());
    }

    private Contacto GetDatosFromJson(String respuesta) throws JSONException {
        JSONObject item = new JSONObject(respuesta);
        return new Contacto(
                item.getString("nombre"),
                item.getString("telefono"),
                item.getString("correo"),
                getResources().getIdentifier(item.getString("img"), "mipmap", this.getPackageName()));
    }
}

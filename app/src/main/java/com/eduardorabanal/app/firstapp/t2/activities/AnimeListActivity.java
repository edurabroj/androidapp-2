package com.eduardorabanal.app.firstapp.t2.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp._practica.adaptadores.AdapterContacto;
import com.eduardorabanal.app.firstapp._practica.entities.Contacto;
import com.eduardorabanal.app.firstapp.t2.adaptadores.AdapterAnime;
import com.eduardorabanal.app.firstapp.t2.entities.Anime;
import com.eduardorabanal.app.firstapp.t2.tasks.AnimeListTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AnimeListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anime_list);

        AnimeListTask at = new AnimeListTask(this);
        at.execute();
    }


    public void LlenarLista(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        ListView lv = (ListView) findViewById(R.id.lv);
        List<Anime> datos=GetDatosFromJson(respuesta);

        AdapterAnime adaptador =
                new AdapterAnime(
                        this,
                        R.layout.item_anime,
                        datos
                );

        lv.setAdapter(adaptador);
    }

    private List<Anime> GetDatosFromJson(String respuesta) throws JSONException {
        List<Anime> datos = new ArrayList<>();
        JSONArray array = new JSONArray(respuesta);

        for (int i=0; i<array.length(); i++){
            JSONObject item = array.getJSONObject(i);

            /*int id = getResources().getIdentifier(item.getString("img"), "mipmap", this.getPackageName());
            Log.i("ID",String.valueOf(id));*/

            datos.add(new Anime(item.getString("imagen"),item.getString("nombre"),item.getString("descripcion"),item.getString("id")));
        }

        return datos;
    }
}
